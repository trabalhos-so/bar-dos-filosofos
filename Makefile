SRC=$(shell find ./src/ -name "*.cpp" -type f)
$(shell mkdir -p objects)
OBJ=$(patsubst ./src%, ./objects%, $(patsubst %.cpp, %.o , $(SRC)))
FLAGS=-Wall -Wextra -Wshadow -Werror -pthread -I include

all: filosofo

filosofo: $(OBJ)
	g++ -o $@ $^ $(FLAGS)

objects/%.o: src/%.cpp
	g++ -c -o $@ $^ $(FLAGS)

run: filosofo
	./filosofo

clean:
	@find . -type f -name "*.o" -exec rm '{}' \;
	@find . -type f -name "filosofo" -exec rm '{}' \;

remake:
	$(MAKE) clean
	$(MAKE)

runmake:
	$(MAKE) remake
	$(MAKE) run

valgrind:
	$(MAKE)
	valgrind --leak-check=full --show-leak-kinds=all ./filosofo