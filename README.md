# Bar dos Filosofos

Generalização do problema Jantar dos Filósofos com grafos arbitrários representando os lugares na mesa;<br>
O grafo deve ser informado em forma de matriz quadrada em um arquivo "grafo.txt".

# Alunos

* Bernardo Dalfovo de Souza - 18204849
* Vinicius Slovinski        - 18201356

# Formato da matriz do grafo

* O tamanho da matriz é definido pelo número de filósofos sentados: Se houverem 10 filósofos a matriz será de tamanho 10x10;
* A divisória de colunas deve ser representada por uma vírgula;
* As linhas e colunas reprentam os vértices (filósofos) do grafo;
* O valor de cada elemento indica se há ligação entre o vértice da linha e o vértice da coluna;
    * 1 indica a existência de ligação;
    * 0 indica a ausência de ligação;
* Para adicionar mais de uma matriz ao arquivo "grafo.txt", separe-as com uma linha vazia;

* Exemplo:

![Exemplo de Grafo](https://i.imgur.com/ijoEv8e.png)

0, 1, 0, 0, 1, //vértice 0 tem ligação com os vértices 1 e 4<br>
1, 0, 1, 0, 0, //vértice 1 tem ligação com os vértices 0 e 2<br>
0, 1, 0, 1, 0, //vértice 2 tem ligação com os vértices 1 e 3<br>
0, 0, 1, 0, 1, //vértice 3 tem ligação com os vértices 2 e 4<br>
1, 0, 0, 1, 0, //vértice 4 tem ligação com os vértices 0 e 3

# Como compilar:

Digite "make all" sem as aspas no terminal quando estiver na pasta do projeto.

# Como executar:

Para executar o programa, digite "make run" sem as aspas.

# Outros comandos "make":

* "make clean" : Limpa os arquivos executaveis e objetos;

* "make remake" : Limpa os arquivos executaveis e objetos, compilando novamente em seguida;

* "make runmake" : Limpa os arquivos executaveis e objetos, compila o programa e executa-o em seguida.

* "make valgrind" : Compila o projeto e roda o valgrind para verificar vazamento de memoria.
