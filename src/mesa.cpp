#include "mesa.hpp"

Mesa* Mesa::inst = nullptr;

Mesa::Mesa()
{
}

Mesa::~Mesa()
{
}


Mesa* Mesa::instanciaSingleton()
{
    if(inst == nullptr)
    {
        inst = new Mesa();
    }
    return inst;
}

void Mesa::setMatrizFilosofos(std::vector<int> novaLinha)
{
    matrizFilosofos.push_back(novaLinha);
}

void Mesa::deleteSingleton()
{
    delete inst;
}

int Mesa::getGarrafasLivres(int idFilosofo)
{
    int cont = 0;
    for(unsigned int i = 0; i < matrizFilosofos[idFilosofo].size(); i++)
    {
        if (matrizFilosofos[idFilosofo][i] != 0) cont++;
    }
    return cont;
}

std::vector<int> Mesa::pegandoGarrafas(int idFilosofo, int qtdDesejada)
{
    std::vector<int> vetorConexoes;
    std::vector<int> vetorIndisponivel;
    std::vector<int> bebidas;
    bool jaReservou = false;

    for(unsigned int index = 0; index < matrizFilosofos[idFilosofo].size(); index++)
    {
        if(matrizFilosofos[index][idFilosofo] == ((idFilosofo+10) * (-1)) ) jaReservou = true;
        if(matrizFilosofos[idFilosofo][index] == -1) vetorIndisponivel.push_back((int)index);
        else if(matrizFilosofos[idFilosofo][index] ==  1) vetorConexoes.push_back((int)index);
    }
    if((int) vetorConexoes.size() < qtdDesejada)
    {
        if(jaReservou == false && ( (int)(vetorIndisponivel.size() + vetorConexoes.size()) >= qtdDesejada))
        {
            int aux = 0;
            while(aux < qtdDesejada)
            {
                if(vetorConexoes.empty())
                {
                    int aleatorio;
                    std::random_device rd;
                    std::mt19937 gen(rd());
                    std::uniform_int_distribution<> dis(0, vetorIndisponivel.size() - 1);
                    aleatorio = dis(gen);
                    matrizFilosofos[vetorIndisponivel[aleatorio]][idFilosofo] = ((idFilosofo+10) * (-1));
                    vetorIndisponivel.erase(vetorIndisponivel.begin() + aleatorio);
                } else {
                    matrizFilosofos[vetorConexoes.back()][idFilosofo] = ((idFilosofo+10) * (-1));
                    vetorConexoes.pop_back();
                }
                aux++;
            }
        }
        return bebidas;
    }

    int bebidasPegas = 0;
    while(bebidasPegas < qtdDesejada)
    {
        int aleatorio, coluna;
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<> dis(0, vetorConexoes.size() - 1);
        aleatorio = dis(gen);
        bebidas.push_back(vetorConexoes[aleatorio]);
        coluna = vetorConexoes[aleatorio];
        matrizFilosofos[coluna][idFilosofo] = -1;
        vetorConexoes.erase(vetorConexoes.begin() + aleatorio);
        bebidasPegas++;
    }

    if(jaReservou == true)
    {
        for(unsigned int i = 0; i < matrizFilosofos.size(); i++)
        {
            if( matrizFilosofos[i][idFilosofo] == ((idFilosofo+10) * (-1)) ) matrizFilosofos[i][idFilosofo] = 1;
        }
    }
    return bebidas;
}

void Mesa::retornandoGarrafas(std::vector<int>vetorGarrafas, int idFilosofo)
{
    while(vetorGarrafas.empty() != true)
    {
        matrizFilosofos[vetorGarrafas.back()][idFilosofo] = 1;
        vetorGarrafas.pop_back();
    }
}

void Mesa::clearMatriz()
{
    matrizFilosofos.clear();
}


