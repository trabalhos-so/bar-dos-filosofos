#include <iostream>
//INCLUDES PARA LER O ARQUIVO TEXTO
#include <fstream>
#include <string>
//
#include <queue>
#include "filosofo.hpp"

void rotinaFilosofo(void* ptr)
{
    ((Filosofo*)ptr)->executaRotina();
}

int main(void)
{
    std::ifstream arquivoGrafo("grafo.txt");
    if (arquivoGrafo.is_open())
    {
        int numeroFilosofos = 0;
        std::vector<Filosofo*> vectorFilosofo;

        std::queue<std::string> queueLinhas;
        std::string linha;
        while( getline(arquivoGrafo, linha) ) queueLinhas.push(linha);
        arquivoGrafo.close();

        while(queueLinhas.empty() == false)
        {
            std::size_t pos = queueLinhas.front().find_first_of("01");
            if(pos != std::string::npos)
            {
                std::vector<int> linhaMatriz;
                int indexColuna = -1;
                while(pos != std::string::npos)
                {
                    indexColuna++;
                    queueLinhas.front().erase(0, pos);
                    linhaMatriz.push_back(std::stoi(queueLinhas.front(), nullptr));
                    queueLinhas.front().erase(0, pos+1);
                    pos = queueLinhas.front().find_first_of("01");
                }
                vectorFilosofo.push_back(new Filosofo(numeroFilosofos));
                Mesa::instanciaSingleton()->setMatrizFilosofos(linhaMatriz);
                queueLinhas.pop();
                if(queueLinhas.empty() == true) queueLinhas.push("");
                numeroFilosofos++;
            } else if(numeroFilosofos > 0){
                std::cout << "\033[0;31m=-=-=-=-=-=-=-=-=-=-=-=-=-=\nGRAFO COM " << numeroFilosofos << " NOS\n=-=-=-=-=-=-=-=-=-=-=-=-=-=\033[0m\n";
                std::vector<std::thread> vectorThread;
                for(int i = 0; i < numeroFilosofos; i++) vectorThread.push_back(std::thread(rotinaFilosofo, vectorFilosofo[i]));
                auto start = std::chrono::high_resolution_clock::now();
                Filosofo::abreBar();
                for(int i = 0; i < numeroFilosofos; i++) vectorThread[i].join();
                auto end = std::chrono::high_resolution_clock::now();
                std::chrono::duration<double, std::milli> elapsed = end - start;
                for(std::vector<Filosofo*>::iterator ptr = vectorFilosofo.begin(); ptr != vectorFilosofo.end(); ptr++)
                {
                    (*ptr)->printResultado();
                    delete *ptr;
                }
                std::cout << "\n\033[1;34mMedia com sede da execucao: \t" << (int)(Filosofo::getMediaFinal() / numeroFilosofos) << " ms\033[0m\n";
                std::cout << "Tempo total da execucao: \t" << elapsed.count() << " ms\n\n";
                vectorFilosofo.clear();
                vectorThread.clear();
                queueLinhas.pop();
                numeroFilosofos = 0;
                Mesa::instanciaSingleton()->clearMatriz();
            } else {
                queueLinhas.pop();
            }
        }
        Mesa::instanciaSingleton()->deleteSingleton();
    } else std::cerr << "\033[0;31mErro ao abrir o arquivo texto\033[0m\n";

    return 0;
}
