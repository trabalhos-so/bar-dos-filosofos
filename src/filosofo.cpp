#include "filosofo.hpp"

using namespace std::chrono_literals;

int Filosofo::maxBebendo;
double Filosofo::mediaTempo;
bool Filosofo::barAberto;

std::mutex Filosofo::garrafaMutex;
std::condition_variable Filosofo::esperaGarrafa;

void Filosofo::abreBar()
{
    barAberto = true;
}

Filosofo::Filosofo(int index)
{
    vezesBebendo = 0;
    barAberto = false;
    if(index <= 5) maxBebendo = 6;
    else maxBebendo = 3;
    idFilosofo = index;
}

void Filosofo::executaRotina()
{
    int maxGarrafas = Mesa::instanciaSingleton()->getGarrafasLivres(idFilosofo);
    tempoTranquilo = 0;
    tempoSede = 0;
    tempoBebendo = 0;
    while(barAberto == false)
    {}
    while(vezesBebendo < maxBebendo)
    {
        garrafasNecessarias = RNG(2, maxGarrafas);
        tranquilo();
        pegaGarrafa();
        bebendo();
        devolveGarrafa();
        vezesBebendo++;
    }
}

void Filosofo::printResultado()
{
    double mediaIndividual = (tempoSede / maxBebendo);
    if(mediaIndividual >= 1.000000) mediaIndividual = (int)mediaIndividual;
    if(tempoSede >= 1.000000) tempoSede = (int)tempoSede;
    if(tempoTranquilo >= 1.000000) tempoTranquilo = (int)tempoTranquilo;
    std::cout << "FILOSOFO " << idFilosofo << "\n\t\033[0;32mTranquilo por \t\t" << tempoTranquilo << " ms\033[0m\n";
    std::cout << "\t\033[0;34mCom sede por \t\t" << tempoSede << " ms\033[0m\n";
    std::cout << "\t\033[0;35mBebendo por \t\t" << (int)tempoBebendo << " ms\033[0m\n\n";
    std::cout << "\t\033[1;34mMedia com sede: \t" << mediaIndividual << " ms\033[0m\n";
    std::cout << "---------------------------------------\n";
    mediaTempo += (tempoSede / maxBebendo);
}

double Filosofo::getMediaFinal()
{
    double temp = mediaTempo;
    mediaTempo = 0;
    return temp;
}

int Filosofo::RNG(int inicio, int fim)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(inicio, fim);
    return dis(gen);
}

void Filosofo::tranquilo()
{
    estadoFilosofo = TRANQUILO;
    auto start = std::chrono::high_resolution_clock::now();
    std::this_thread::sleep_for(RNG(0,2000) * 1ms);
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> elapsed = end - start;
    tempoTranquilo += elapsed.count();
}

void Filosofo::pegaGarrafa()
{
    Mesa* instMesa = Mesa::instanciaSingleton();
    estadoFilosofo = COM_SEDE;
    auto start = std::chrono::high_resolution_clock::now();
    
    std::unique_lock<std::mutex> garrafaLock(garrafaMutex, std::defer_lock);
    while(estadoFilosofo == COM_SEDE)
    {
        garrafaLock.lock();
        garrafasPegas = instMesa->pegandoGarrafas(idFilosofo, garrafasNecessarias);
        if(garrafasPegas.empty() == true)
        {
            esperaGarrafa.wait(garrafaLock);
        }else{
            estadoFilosofo = BEBENDO;
        }     
        garrafaLock.unlock();
    }
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> elapsed = end - start;
    tempoSede += elapsed.count();
}

void Filosofo::bebendo()
{
    auto start = std::chrono::high_resolution_clock::now();
    std::this_thread::sleep_for(1s);
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> elapsed = end - start;
    tempoBebendo += elapsed.count();
}

void Filosofo::devolveGarrafa()
{
    Mesa::instanciaSingleton()->retornandoGarrafas(garrafasPegas, idFilosofo);
    esperaGarrafa.notify_all();
    garrafasPegas.clear();
}