#ifndef GARRAFA_HPP
#define GARRAFA_HPP


#include <vector>
#include <iostream>
#include <random>

class Mesa
{
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                    CLASSE MESA
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
    public:
        ~Mesa();
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                DESTRUTOR PADRÃO
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
        static Mesa* instanciaSingleton();
        Mesa(Mesa &other) = delete;
        void operator=(const Mesa &) = delete;
        void deleteSingleton();
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                MÉTODOS SINGLETON

        instanciaSingleton....retorna a instancia estatica da classe Singleton
        Construtor............construtor deve ser apenas chamado pelo metodo acima
        operator=.............instancia Singleton nao pode ter valor atribuido
        deleteSingleton.......deleta a instancia singleton no fim do programa
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
        void setMatrizFilosofos(std::vector<int>);
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                MÉTODO SETMATRIZFILOSOFO

        Esse método produz a matriz que foi lida no arquivo para a classe Mesa.
        Todas as operações dos filósofos alteraram essa matriz,
        pois ela fornece todas as conexões entre filósofos. 
        Cada filósofo tem seu ID referente a linha na matriz, e cada thread
        envia sua linha da matriz como vector<int>. 
        Para checar suas conexões é só conferir a linha desse filósofo e para fazer alterações,
        como pegar ou entregar bebidas, deve-se alterar a coluna desse filósofo assim
        alterando as linhas dos filósofos que tem conexão com ele.
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
        int getGarrafasLivres(int);
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                MÉTODO GETGARRAFASLIVRES

        Esse método recebe o ID do filósofo referente a linha dele na matriz.
        Então essa linha é lida, se existir conexões, ou seja, número diferente
        de 0 na conexão, o contador de garrafas é incrementado em um. Se o número lido for 
        0 significa que não há conexão entre esses filósofos (número da linha e da coluna) 
        e se o número for -1 significa que essa conexão ou garrafa está sendo utilizada.
        Esse método retorna na verdade a quantidade de conexões que cada filosofo tem.
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
        std::vector<int> pegandoGarrafas(int ,int);
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                MÉTODO PEGANDO GARRAFAS

        Método responsável por fazer a alteração na matriz relativa ao uso da garrafa.
        Recebe o ID ou linha do filósofo e a quantidade de garrafas que ele gostaria de utilizar.
        O método lê a linha do filósofo e coloca no vetorConexoes o índice da coluna caso o
        conteúdo dessa posição seja 1, coloca no vetorIndisponivel o índice da coluna caso o
        conteúdo dessa posição seja -1. E confere na coluna do filósofo se ele já reservou.
        Caso não tenha garrafas disponíveis suficiente, e caso o filósofo não tenha reservado
        ele irá fazer uma reserva. A reserva consiste em inserir uma identificação única para
        cada filósofo em todas as suas garrafas disponíveis (vetorConexoes) e nas indisponíveis
        (vetorIndisponiveis) até que a quantidade desejada seja atingida.
        Fazendo isso ele garante prioridade por ter chegado primeiro.
        O vetor contendo as posições que foram alteradas é retornado.
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
        void retornandoGarrafas(std::vector<int>, int);
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                MÉTODO RETORNANDOGARRAFAS

        Esse método recebe o vetor de garrafas pegas pelo filósofo e a identificação.
        Ao retirar o número ele limpa a posição no vetor, e só quando devolver todas as garrafas,
        ou seja, o vetor estiver vazio o método encerra. A matriz nas posições do vetor está setada
        como -1 antes desse método, simbolizando garrafa utilizada, então para devolvê-las
        deve-se apenas alterá-las para 1.
        Ex.: número retirado do vetor é 2, matrizFilosofos[2][idFilosofo] = 1.
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
        void clearMatriz();
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                MÉTODO CLEARMATRIZ

        Chamado após todas as threads finalizarem suas rotinas, ou seja, a matriz antiga
        não vai ser mais necessária e deve ser excluída para que a nova ocupe seu lugar.
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
    private:
        Mesa();
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                CONSTRUTOR PADRÃO

        Chamado apenas uma vez pelo metodo instanciaSingleton
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
        std::vector<std::vector<int>> matrizFilosofos;
        
    protected:
        static Mesa* inst;
};

#endif