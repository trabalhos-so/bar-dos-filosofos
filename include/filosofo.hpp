#ifndef FILOSOFO_HPP
#define FILOSOFO_HPP

#include <vector>
#include <iostream>
#include <random>
#include <chrono>
#include <mutex>
#include <thread>
#include <condition_variable>
#include <string>

#include "mesa.hpp"

class Filosofo
{
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                CLASSE FILOSOFO
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
    public:
        Filosofo(int);
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                CONSTRUTOR
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
        void executaRotina();
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                MÉTODO EXECUTAROTINA

        Responsável por controlar o caminho de cada thread.
        Chama os métodos que a thread deve desempenhar na ordem correta até
        atingir o número de vezes estipulado.
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
        void printResultado();
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                MÉTODO PRINTRESULTADO

        Quando todas as threads terminarem suas rotinas esse método é chamado,
        para então mostrar os valores obtidos durante o processo de
        compartilhamento de garrafas, com dados sobre os tempos de cada filósofo.
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
        static double getMediaFinal();
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                MÉTODO GETMEDIAFINAL

        Retorna média final dos tempos que os filósofos ficaram no estado
        COM SEDE durante toda a rotina.
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
        static void abreBar();
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                MÉTODO ABREBAR

        Metodo que libera a entrada de todas as threads juntas,
        para que todas comecem a rotina com um delay menor.
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
    private:

        void tranquilo();
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                MÉTODO TRANQUILO

        Responsável por alterar o estado do filósofo para tranquilo.
        Ao fazer isso sorteia um tempo aleatório para cada filósofo que passa
        por esse método, ou seja, cada filósofo irá ficar por um tempo
        diferente no estado tranquilo. Esse tempo varia entre 0 e 2000ms.
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
        void pegaGarrafa();
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                MÉTODO PEGAGARRAFA

        Este método controla o compartilhamento de garrafas. Nele o filósofo
        muda seu estado para COM SEDE. Devido ao mutex, um filósofo por vez
        tenta pegar a garrafa chamando o método pegandoGarrafas da classe mesa.
        Caso ele não consiga, fica esperando um sinal que só é enviado quando
        um filósofo entregar as garrafas que pegou, somente então, quem está
        esperando tenta pegar novamente.
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
        void devolveGarrafa();
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                MÉTODO DEVOLVEGARRAFA

        Depois de beberem, os filósofos entram nesse método para entregarem
        suas garrafas. Chamam então o método retornandoGarrafas da classe mesa, 
        enviando o vetor contendo os índices das garrafas que ele pegou.
        O conteúdo desse vetor é por fim limpo.
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
        void bebendo();
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                MÉTODO BEBENDO

        Realiza o tempo de espera de 1 segundo para o filosofo tomar sua
        garrafa de bebida.
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
        int RNG(int, int);
        /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                MÉTODO RNG

        Gerador de numeros aleatorios. Utilizado para randomizar a escolha
        de garrafas e o tempo de espera no estado TRANQUILO.
        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

        enum estados {TRANQUILO, COM_SEDE, BEBENDO};
        estados estadoFilosofo;

        static double mediaTempo;
        double tempoTranquilo;
        double tempoSede;
        double tempoBebendo;

        int idFilosofo;
        int garrafasNecessarias;
        int vezesBebendo;
        std::vector<int> garrafasPegas;

        static int maxBebendo;
        static bool barAberto;

        static std::mutex garrafaMutex;
        static std::condition_variable esperaGarrafa;
};

#endif